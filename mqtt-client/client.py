########################################################################################
#           Airgap Networks, Confidential
#
#      [2019] - [2021] Airgap Networks, Incorporated
#                All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Airgap Networks Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Airgap Networks Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Airgap Networks Incorporated.
#
# Author: Satish, Mohan
# Email : satish@airgap.io
########################################################################################

import os
import uuid
import re
import time
import subprocess
import threading
import paho.mqtt.client as mqtt
from jinja2 import FileSystemLoader, Environment
import stat
import locale
import ipaddress
import shutil
from os import path
import yaml
import json
import datetime
import ssl
import logging
from mgmt_controller import MgmtControllerClient
from config import CONFIG, API_FOR_URLS, auth_headers, get_params

def synchronized(f):
    """ Synchronization decorator. """

    def newFunction(self, *args, **kw):
        self._lock.acquire()
        try:
            return f(self, *args, **kw)
        finally:
            self._lock.release()
    return newFunction


# Client class to handle MQTT subscriptions
class SGWClient():
    def __init__(self, local_db_name=None):
        self._lock = threading.Lock()
        self._mgmt_controller = MgmtControllerClient()
        self._mgmt_controller.config_urls()
        self.mqtt_client = None
        self.cluster_topic = None
        logging.basicConfig(level=logging.DEBUG)

    def get_mqtt_client(self):
        if self.mqtt_client:
            return self.mqtt_client
        try:
            # login and get authorization token to use for rest api
            if self._mgmt_controller.get_authorization_token() is None:
                logging.error(f'get_mqtt_client: failed to get authorization token')
                return None

            # There is a separate MQTT Endpoint for subscriptions. Use an api to query the endpoint.
            mqtt_endpoint = self._mgmt_controller.get_mqtt_endpoint()
            if mqtt_endpoint is None:
                logging.warning(f'get_mqtt_client: unable to retrieve mqtt_endpoint information')
                return None

            # MQTT requires a unique client id for each API client
            client_id = str(uuid.uuid1())
            client = mqtt.Client(client_id, clean_session=False)
            if not client:
                logging.error(f'get_mqtt_client: unable to create mqtt client')
                return None

            # Setup callback handlers
            client.on_connect = self.on_mqtt_connect
            client.on_subscribe = self.on_mqtt_subscribe
            client.on_unsubscribe = self.on_mqtt_unsubscribe
            client.on_message = self.on_mqtt_message

            ## The management controller sends the mqtt endpoint tcp url as: tcp://<dns-name>:1883/mqtt
            ## Let us extract the base url out of it
            p = '(?:tcp.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'

            m = re.search(p, mqtt_endpoint['result']['tcp_url'])
            host = m.group('host') # for example 'www.abc.com'
            port = int(m.group('port')) # for example, int('123')

            # The password to use for MQTT connection is the JWT (authentication taken) which we derive
            # from authenticating to the management system
            client.username_pw_set(username=mqtt_endpoint['result']['user_name'],
                                   password=self._mgmt_controller._auth_token)
            client.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
                           tls_version=ssl.PROTOCOL_TLS, ciphers=None)
            
            # initiate connection to the MQTT broker and start event processing loop
            # NOTE: The event processing runs in a separate thread.
            client.connect(host, port, 60)
            logging.debug(f'get_mqtt_client: initiated connection to {host} at {port}')
            self.mqtt_client = client
            return client
        except Exception as e:
            logging.error(f'Failed to connect to mqtt endpoint {e}', exc_info=True)

    # Connect to MQTT endpoint and loop for events
    @synchronized
    def subscribe_mqtt(self):
        if self.mqtt_client:
            return
        try:
            self.mqtt_client = self.get_mqtt_client()
            if self.mqtt_client:
                logging.debug(f'subscribe_mqtt: starting event loop...')
                self.mqtt_client.loop_start()
        except Exception as e:
            logging.error(f'Failed to start mqtt loop {e}', exc_info=True)

    # The callback for when the client receives a connection response from the server.
    def on_mqtt_connect(self, client, userdata, flags, rc):
        if rc != 0:
            logging.error(f"on_mqtt_connect: failed to connect to MQTT broker with result code {rc}")
            return

        # Subscribing in on_mqtt_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        logging.debug('on_mqtt_connect: connection succeeded, renewing subscriptions')

        # Subscribe to cluster and network real-time events
        if self.cluster_topic == None:
            # Topic name is in the format: <mssp-name>.<customer-name>.<event-id>
            mssp_name = CONFIG.get("mssp_name")
            tenant_name = CONFIG.get("tenant_name")

            self.cluster_topic = '.'.join((mssp_name, tenant_name, 'cluster-events'))
            self.network_topic = '.'.join((mssp_name, tenant_name, 'network-events'))

        topics = [ (self.cluster_topic, 0), (self.network_topic, 1) ]
        rc, mid = client.subscribe(topics)
        logging.debug(f'on_mqtt_connect: subscribed to {topics}, rc {rc}, mid {mid}')
        if rc != 0:
            logging.error(f'on_mqtt_connect: subscribed to {topics}, rc {rc}, mid {mid}')

    # subscription status callback
    def on_mqtt_subscribe(self, client, userdata, mid, granted_qos):
        logging.debug(f'on_mqtt_subscribe: client {client} userdata {userdata} mid {mid} qos {granted_qos}')

    # unsubscribe callback
    def on_mqtt_unsubscribe(self, client, userdata, mid):
        logging.debug('on_mqtt_unsubscribe: client {client} userdata {userdata} mid {mid}')

    # The callback for when a PUBLISH message is received from the server.
    def on_mqtt_message(self, client, userdata, msg):
        logging.debug(f'on_mqtt_message: topic {msg.topic} with payload: {str(msg.payload)}')

        # convert bytes array into utf-8 string
        payload_str = msg.payload.decode('utf-8')
        payload = json.loads(payload_str)

        if msg.topic == self.cluster_topic:
            self.handle_mqtt_cluster_event(payload)
        elif msg.topic == self.network_topic:
            logging.debug(f'on_mqtt_message: Network Event {payload}')
            pass
        else:
            logging.error(f'on_mqtt_message: unknown topic {msg.topic}')

    def handle_mqtt_cluster_event(self, payload):
        # This is the data we receive in the cluster-events message {"EventType":2,"ClusterID":214}
        logging.debug(f'handle_mqtt_cluster_event: received cluster-event message {payload}')

        ## query cluster and gateway states to get the current status
        gateways = self._mgmt_controller.get_gateways()
        if gateways is not None:
            logging.debug(f'Querying gateways {gateways}')