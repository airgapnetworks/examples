########################################################################################
#           Airgap Networks, Confidential
#
#      [2019] - [2021] Airgap Networks, Incorporated
#                All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Airgap Networks Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Airgap Networks Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Airgap Networks Incorporated.
#
# Author: Satish, Mohan
# Email : satish@airgap.io
########################################################################################

import time
import os
import tempfile
import pkg_resources
import requests as reqs
from http import HTTPStatus
import base64
import json
import logging
from config import CONFIG, API_FOR_URLS, auth_headers, get_params

logging.basicConfig(level=logging.DEBUG)

# Abstraction class to interact with the Airgap management system. This class abstracts
# the REST API calls into easy to use functions.
class MgmtControllerClient(object):
    def __init__(self):
        self._api_endpoint_url = None
        self._auth_token = None
        self.log_level = None
        self._login_url = None
        self._get_cluster_gateways_url = None
        self._get_cluster_by_name_url = None
        self._get_gateways_url = None
        self._get_gateway_by_id_url = None
        self._get_networks_url = None
        self._get_gateway_by_name_url = None
        self._get_network_by_name_url = None
        self._get_site_by_name_url = None
        self._get_customer_config_url = None
        self._last_auth_time = None
        self._get_groups_url = None
        self._get_group_membership_url = None
        self._get_group_membership_last_update_time_url = None
        self._get_group_policies_url = None
        self._get_group_policies_last_update_time_url = None
        self._get_rks_state = None
        self._get_mssp_name = None
        self._get_mqtt_endpoint_url = None

    def _do_request(self, request_function, *args, **kwargs):
        num_tries = 0
        function_name = f"{request_function.__module__}.{request_function.__name__}"
        while True:
            num_tries += 1
            response = request_function(*args, **kwargs)
            if response.status_code != HTTPStatus.OK:
                logging.debug(
                    f"{function_name} {args} {kwargs} got an error code {response.status_code}")
                if num_tries > 1:
                    return response
                if response.status_code == HTTPStatus.UNAUTHORIZED:
                    self.get_authorization_token(force=True)
                    # Rety through the loop one more time; hopefully token has been refreshed
            else:
                return response

    
    def config_urls(self):
        self._api_endpoint_url = CONFIG.get('api_endpoint_url')
        self._auth_email = CONFIG.get('auth_email'),
        self._auth_password = CONFIG.get('auth_password')
        self._login_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('login')}"
        self._get_gateways_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_gateways')}"
        self._get_gateway_by_id_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_gateway_by_id')}"
        self._get_cluster_gateways_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_cluster_gateways')}"
        self._get_cluster_by_name_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_cluster_by_name')}"
        self._get_networks_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_networks')}"
        self._get_network_by_name_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_network_by_name')}"
        self._get_site_by_name_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_site_by_name')}"
        self._get_gateway_by_name_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_gateway_by_name')}"
        self._get_customer_config_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_customer_config')}"
        self._consume_dhcp_events_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('consume_dhcp_events')}"
        self._get_groups_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_groups')}"
        self._get_group_membership_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_group_membership')}"
        self._get_group_membership_last_update_time_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_group_membership_last_update_time')}"
        self._get_group_policies_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_group_policies')}"
        self._get_group_policies_last_update_time_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_group_policies_last_update_time')}"
        self._get_mqtt_endpoint_url = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_mqtt_endpoint')}"
        self._get_rks_state = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_rks_state')}"
        self._get_mssp_name = f"{self._api_endpoint_url}{API_FOR_URLS.get('get_mssp_name')}"
    
    def authorized(self):
        return self._auth_token != None

    def get_authorization_token_data(self):
        return {
            'email': CONFIG.get("auth_email"),
            'password': CONFIG.get("auth_password")
        }

    def get_authorization_token(self, force=False):
        try:
            if not self._last_auth_time:
                self._last_auth_time = time.time()
            elif not force and (self._auth_token and time.time() - self._last_auth_time < 300):
                return self._auth_token
            auth_response = reqs.post(
                self._login_url,
                data=self.get_authorization_token_data(),
                timeout=CONFIG.get('requests_timeout')
            )
            if not auth_response:
                return None
            if auth_response.status_code != HTTPStatus.OK:
                logging.debug(
                    f"get_authorization_token got an error code {auth_response.status_code}")
                return None
            json_response = auth_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            if not result:
                return None
            self._auth_token = result.get('token')
            self._last_auth_time = time.time()
            return self._auth_token
        except Exception as e:
            logging.error(
                f'Failed to get authorization token {e}', exc_info=True)
            return None

    def get_cluster_by_name(self, site_name, cluster_name):
        try:
            params = [('site_name', site_name), ('cluster_name', cluster_name)]
            cluster_response = self._do_request(
                reqs.get,
                f'{self._get_cluster_by_name_url}',
                headers=auth_headers(self._auth_token),
                params=get_params(params),
                timeout=CONFIG.get('requests_timeout'),
            )
            if not cluster_response:
                return None
            json_response = cluster_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            logging.info(f'get_cluster_by_name -- Cluster from MANO {result} ')
            return result
        except Exception as e:
            logging.error(
                f'Failed to get cluster  {e}', exc_info=True)
            return None

    def get_cluster_gateways(self, site_name, cluster_name):
        params = [('site_name', site_name), ('cluster_name', cluster_name)]
        try:
            cluster_gateways_response = self._do_request(
                reqs.get,
                f'{self._get_cluster_gateways_url}',
                headers=auth_headers(self._auth_token),
                params=get_params(params),
                timeout=CONFIG.get('requests_timeout'),
            )
            if not cluster_gateways_response:
                return None
            json_response = cluster_gateways_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            if not result:
                return None
            return result.get('gateways')
        except Exception as e:
            logging.error(
                f'Failed to get cluster gateways {e}', exc_info=True)
            return None

    def get_customer_config(self):
        try:
            customer_config_response = self._do_request(
                reqs.get,
                f'{self._get_customer_config_url}',
                headers=auth_headers(self._auth_token),
                timeout=CONFIG.get('requests_timeout')
            )
            if not customer_config_response:
                return None
            json_response = customer_config_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            return result
        except Exception as e:
            logging.error(
                f'Failed to get customer config {e}', exc_info=True)
            return None

    def get_site_by_name(self, site_name):
        try:
            if not site_name:
                logging.error("get_site_by_name: site name not provided")
                return None
            site_response = self._do_request(
                reqs.get,
                f'{self._get_site_by_name_url}/{site_name}',
                headers=auth_headers(self._auth_token),
                timeout=CONFIG.get('requests_timeout')
            )
            if not site_response:
                logging.error(f'get_site_by_name() site_response is None {site_response}')
                return None
            json_response = site_response.json()
            if not json_response:
                return None
            return json_response
        except Exception as e:
            logging.error(
                f'Failed to get site information {e}', exc_info=True)
            return None

    def get_gateways(self):
        try:
            gateways_response = self._do_request(
                reqs.get,
                f'{self._get_gateways_url}',
                headers=auth_headers(self._auth_token),
                params=get_params([('page', -1), ('gateway_type', 'isolation')]),
                timeout=CONFIG.get('requests_timeout')
            )
            if not gateways_response:
                return None
            json_response = gateways_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            if not result:
                return None
            return result.get('rows')
        except Exception:
            logging.error('Failed to get gateway', exc_info=True)
            return None

    def get_gateway(self, gateway_id):
        try:
            if not gateway_id:
                logging.error("get_gateway: no gateway id provided")
                return None
            gateways_response = self._do_request(
                reqs.get,
                f'{self._get_gateway_by_id_url}/{gateway_id}',
                headers=auth_headers(self._auth_token),
                params=get_params([('page', -1)]),
                timeout=CONFIG.get('requests_timeout'),
            )
            if not gateways_response:
                return None
            json_response = gateways_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            return result
        except Exception as e:
            logging.error(
                f'Failed to get gateway by id {e}', exc_info=True)
            return None

    def get_gateway_id(self, gateway_name):
        try:
            if not gateway_name:
                logging.error("get_gateway_id: no gateway name provided")
                return None
            gateway_id_url = self._get_gateway_by_name_url + f"/{gateway_name}"
            get_gateway_by_name_response = self._do_request(
                reqs.get,
                gateway_id_url,
                headers=auth_headers(self._auth_token),
                timeout=CONFIG.get('requests_timeout')
            )
            json_response = get_gateway_by_name_response.json()
            if not json_response:
                return None
            result = json_response.get('result')
            if not result:
                return None
            return result.get('gateway_id')
        except Exception as e:
            logging.error(
                f'Failed to get gateway_id {e}', exc_info=True)
    
    def get_mqtt_endpoint(self):
        try:
            mqtt_endpoint = self._do_request(
                reqs.get,
                f'{self._get_mqtt_endpoint_url}',
                headers=auth_headers(self._auth_token),
                timeout=CONFIG.get('requests_timeout')
            )

            if mqtt_endpoint is None:
                logging.debug(
                    f"get_mqtt_endpoint: Endpoint returned is None")
                return None
            
            if mqtt_endpoint.status_code != 200:
                logging.error(
                    f"get_mqtt_endpoint: Failed to get mqtt_endpoint: {mqtt_endpoint.status_code}")
                return None

            json_response = mqtt_endpoint.json()
            if not json_response:
                return None
            return json_response
        except Exception as e:
            logging.error(
                f'Failed to get mqtt_endpoint for {e}', exc_info=True)
            return None
    

    def get_mssp_name(self):
        try:
            response = self._do_request(
                reqs.get,
                f'{self._get_mssp_name}',
                headers=auth_headers(self._auth_token),
                timeout=CONFIG.get('requests_timeout')
            )
            if response == None:
                logging.error(f"get_mssp_name failed")
                return None
            json_response = response.json()
            if json_response == None:
                logging.error(f"get_mssp_name json decode failed")
                return None
            return json_response['result']
        except Exception as e:
            logging.error(
                f'get_mssp_name: Failed to get MSSP name: {e}', exc_info=True)
            return None
