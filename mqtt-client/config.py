#! /usr/bin/env python3
########################################################################################
#           Airgap Networks, Confidential
#
#      [2019] - [2021] Airgap Networks, Incorporated
#                All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Airgap Networks Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Airgap Networks Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Airgap Networks Incorporated.
#
# Author: Satish, Mohan
# Email : satish@airgap.io
########################################################################################

from site_config import SiteConfig

# A collection of Airgap APIs of interest (note that this is not an exhaustive list)
API_FOR_URLS = dict(
    login='/api/v2/auth/login',
    get_gateways='/api/v2/Gateway/',
    get_cluster_by_name='/api/v2/cluster/clusterByName/get',
    get_cluster_gateways='/api/v2/ClusterGateway/get',
    get_gateway_by_id='/api/v2/Gateway/id',
    get_gateway_by_name='/api/v2/Gateway/name',
    get_network_by_name='/api/v2/Network/networkByName',
    get_networks='/api/v2/Network',
    get_site_by_name='/api/v2/Site/siteByName',
    get_customer_config='/api/v2/Customer/config',
    get_compromised_devices_url='/api/v2/CompromisedDevice/insert',
    consume_dhcp_events='/api/v2/dhcp-events',
    alarm='/api/v2/alarm',
    get_groups='/api/v2/groups',
    get_group_membership='/api/v2/group-membership',
    get_group_membership_last_update_time='/api/v2/group-membership/last-update-time',
    get_group_policies='/api/v2/group-policy-rules',
    get_group_policies_last_update_time='/api/v2/group-policy-rules/last-update-time',
    put_appgw = '/api/v3/applicationgateway/id',            # update one entry
    get_mqtt_endpoint = '/api/v3/settings/mqtt-broker-url',
    get_rks_state = '/api/v3/ransomware-kill/state/',
    get_mssp_name ='/api/v2/Customer/mssp_name',
)

CONFIG = dict(
    # timeout for http requests (10 seconds)
    requests_timeout=10,
    network_mask=30
)


def auth_headers(authorized_token):
    if not authorized_token:
        authorized_token = "<None>"
    return {'accept': 'application/json', 'authorization': 'Bearer ' + authorized_token}


def get_params(params: list):
    dict_params = dict(page=-1)
    for key, value in params:
        dict_params[key] = value
    return dict_params

def chain_configs(*config_items):
    for it in config_items:
        for element in it:
            value = element[1]
            if value != None:
                yield element

siteConfig = SiteConfig()
CONFIG = dict(chain_configs(CONFIG.items(), siteConfig.read_site_config().items()))
