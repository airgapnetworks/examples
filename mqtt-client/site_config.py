########################################################################################
#           Airgap Networks, Confidential
# 
#      [2019] - [2021] Airgap Networks, Incorporated
#                All Rights Reserved.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Airgap Networks Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Airgap Networks Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Airgap Networks Incorporated.
#
# Author: Satish, Mohan
# Email : satish@airgap.io 
########################################################################################

import json
import os
import logging

logging.basicConfig(level=logging.DEBUG)

# Read the configuration file and return contents as a dict
class SiteConfig(object):
    def __init__(self):
        self.server_url = None
        self.auth_email = None
        self.auth_password = None
        self._site_config = None

    def available(self):
        return self._site_config != None and \
            self.server_url != None and \
            self.auth_email != None

    def check_site_config_availability(self):
        # Always read the configuration, just in case it has changed for it to take effect
        try:
            if os.path.getmtime(self.site_config_file_name()) == self._site_config_mtime:
                return self.available()
        except Exception as e:
            logging.info(f"check_site_config_availability: {e}")
        self._read_site_config()
        return self.available()

    def site_config_file_name(self):
        return os.path.join(".", "config.json")

    def read_site_config(self):
        try:
            logging.info(f"Reading site configuration from {self.site_config_file_name()}")
            with open(self.site_config_file_name()) as site_config_file:
                self._site_config = json.load(site_config_file)

            # save a few key arguments
            self._site_config_mtime = os.path.getmtime(self.site_config_file_name())
            self.auth_email = self._site_config.get('auth_email')
            self.auth_password = self._site_config.get('auth_password')
            self.server_url = self._site_config.get('server_url')
            return self._site_config
        except Exception as e:
            self._site_config = None
            logging.info(f'Failed to open {self.site_config_file_name()} : {e}',
                              exc_info=True)