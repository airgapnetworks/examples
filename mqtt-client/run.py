########################################################################################
#           Airgap Networks, Confidential
#
#      [2019] - [2021] Airgap Networks, Incorporated
#                All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Airgap Networks Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Airgap Networks Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Airgap Networks Incorporated.
#
# Author: Satish, Mohan
# Email : satish@airgap.io
########################################################################################

import time
import logging
from client import SGWClient

client = SGWClient()
logging.basicConfig(level=logging.DEBUG)

def handle_mqtt_events():
    try:
        client.subscribe_mqtt()
    except Exception as e:
        logging.error(f"Exception encountered in handle_mqtt_events {e}", exc_info=True)

# The main entry point - handle mqtt events for 2 minutes and then exit
def main():
    handle_mqtt_events()
    # process events for 2 minutes and then exit
    time.sleep(120)

if __name__ == "__main__":
    main()
