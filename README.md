# README #

This README file documents how to run the sample examples which illustrate how to use the Airgap APIs. The first example provided is the `mqtt-client`, which subscribes
to a real-time stream of events and queries other APIs to discover the current state of gateways and other entities.

## Installing and running the MQTT Client

There is a configuration file `config.json` under the `mqtt-client` subdirectory. This needs to be populated before running the `mqtt-client` application. A sample
configuration file is shown below. Set the `api_endpoint_url` property to the appropriate endpoint corresponding to your MSSP instance. For instance if your Airgap portal is `qa.goairgap.com`,
the API endpoint would be `qa-api.goairgap.com` and the `mssp_name` would be `qa`.

The current example illustrates subscribing to events for a single tenant, but it would be simple to extend this application to multiple tenants (or use MSSP credentials). Create an
admin account in the Airgap portal with the appropriate RBAC role, and set its credentials in the configuration file as shown below. This is used by the sample application to authenticate
to the Airgap API service and register for subscriptions.

```
{
    "api_endpoint_url":"https://qa-api.goairgap.com",
    "mssp_name":"qa",
    "tenant_name":"earth",
    "auth_email":"apiclient@airgap.io",
    "auth_password":"ip8CIoB7z9"
}
```

After the configuration is completed, the application may be run as follows (it needs Python3 to run this application).

```
cd mqtt-client
pip3 install -r requirements.txt
python3 run.py
```

A sample run in our internal QA environment shows the following output:

```
NFO:root:Reading site configuration from ./config.json
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "POST /api/v2/auth/login HTTP/1.1" 200 850
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "GET /api/v3/settings/mqtt-broker-url HTTP/1.1" 200 129
DEBUG:root:get_mqtt_client: initiated connection to qa-mqtt.goairgap.com at 1883
DEBUG:root:subscribe_mqtt: starting event loop...
DEBUG:root:on_mqtt_connect: connection succeeded, renewing subscriptions
DEBUG:root:on_mqtt_connect: subscribed to [('qa.earth.cluster-events', 0), ('qa.earth.network-events', 1)], rc 0, mid 1
DEBUG:root:on_mqtt_subscribe: client <paho.mqtt.client.Client object at 0x7f83cf745208> userdata None mid 1 qos (0, 1)
DEBUG:root:on_mqtt_message: topic qa.earth.cluster-events with payload: b'{"EventType":1,"ClusterID":214}'
DEBUG:root:handle_mqtt_cluster_event: received cluster-event message {'EventType': 1, 'ClusterID': 214}
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "GET /api/v2/Gateway/?page=-1&gateway_type=isolation HTTP/1.1" 200 None
DEBUG:root:Querying gateways [{'gateway_id': '156622c4-7a99-4cb8-a1b8-f01c1337ec7d', 'cluster_id': 225, 'desired_state': 'standalone', 'operational_state': 'standalone', 'gateway_name': 'aaa', 'control_plane_ip': '203.0.113.0', 'control_pNFO:root:Reading site configuration from ./config.json
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "POST /api/v2/auth/login HTTP/1.1" 200 850
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "GET /api/v3/settings/mqtt-broker-url HTTP/1.1" 200 129
DEBUG:root:get_mqtt_client: initiated connection to qa-mqtt.goairgap.com at 1883
DEBUG:root:subscribe_mqtt: starting event loop...
DEBUG:root:on_mqtt_connect: connection succeeded, renewing subscriptions
DEBUG:root:on_mqtt_connect: subscribed to [('qa.earth.cluster-events', 0), ('qa.earth.network-events', 1)], rc 0, mid 1
DEBUG:root:on_mqtt_subscribe: client <paho.mqtt.client.Client object at 0x7f83cf745208> userdata None mid 1 qos (0, 1)
DEBUG:root:on_mqtt_message: topic qa.earth.cluster-events with payload: b'{"EventType":1,"ClusterID":214}'
DEBUG:root:handle_mqtt_cluster_event: received cluster-event message {'EventType': 1, 'ClusterID': 214}
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): qa-api.goairgap.com:443
DEBUG:urllib3.connectionpool:https://qa-api.goairgap.com:443 "GET /api/v2/Gateway/?page=-1&gateway_type=isolation HTTP/1.1" 200 None
DEBUG:root:Querying gateways [{'gateway_id': '156622c4-7a99-4cb8-a1b8-f01c1337ec7d', 'cluster_id': 225, 'desired_state': 'standalone', 'operational_state': 'standalone', 'gateway_name': 'aaa', 'control_plane_ip': '203.0.113.0', ...
```
